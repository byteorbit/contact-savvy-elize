var contactController = (function(){

    var Contact = function(id, name, surname, number, email, imgUrl){
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.number = number;
        this.email = email;
        this.imgUrl = imgUrl;
    };

    Contact.prototype.setCategory = function(el){
        //category is equal to first letter of surname
        this.category = (el.substring(1, 0)).toUpperCase();
        return this.category;
    };

    var data = [];

    //create some static Contacts to have some population on page (representing a DB)
    var contact1 = new Contact(1, "Ben", "Nevis", "0821232123", "email@place.com", "static/img/user5.jpg");
    var contact2 = new Contact(2, "Brian", "Clayton", "0821232123", "email@place.com", "static/img/user2.jpg");
    var contact3 = new Contact(3, "Brittney", "Klerk", "0821232123", "email@place.com", "static/img/user3.jpg");
    var contact4 = new Contact(4, "Casandra", "Harris", "0821232123", "email@place.com", "static/img/user4.jpg");
    var contact5 = new Contact(5, "Caitlyn", "Bowens", "0821232123", "email@place.com", "static/img/user1.jpg");


    var setCategories = function(arr) {
        arr.forEach(function(current){
            current.setCategory(current.surname);
        })
    };

    data.push(contact1, contact2, contact3, contact4, contact5);
    // create categories for existing data
    setCategories(data);

    var getAllCategories = function(arr) {
        // get all categories of all contacts
        // ?? why is Webstorm saying this variable 'catArray' is redundant.
        var catArray = arr.map(function(current){
            return current.category;
        });
        return catArray;
    };

    var createUniqueCategories = function(array){
        // loop through all and create array with only unique categories (removing duplicate values)
        var uniqueArray = [];
        for(var i = 0; i < array.length; i++){
            if(uniqueArray.indexOf(array[i]) === -1) {
                uniqueArray.push(array[i]);
            }
        }

        // re-order array into alphabetical order
        uniqueArray.sort(function(a, b) {
            return a === b ? 0 : a < b ? -1 : 1;
        });
        return uniqueArray;
    };

    return {
        addContact: function(obj){
            var ID, newContact, imageUrl;

            //find ID of last element of array, and add 1 for new unique ID.
            if(data.length) {
                ID = data[data.length - 1].id + 1;
            } else {
                ID = 1;
            }

            newContact = new Contact(ID, obj.name, obj.surname, obj.number, obj.email, obj.imgUrl);
            newContact.setCategory(obj.surname);

            //add new contact to data:
            data.push(newContact);

            // console.log('New Contact:');
            // console.log(newContact);

            return newContact;
        },

        removeContact: function(id){
            var idArr, index;
            idArr = data.map(function(current){
                return current.id;
            });

            //console.log('id received to map and index '+ id);
            //console.log('removeContact idArr ' + idArr);

            // now we find the index of the id on the array
            index = idArr.indexOf(id);

            //console.log('index of idArr ' + index);

            // remove item from data;
            if(index > -1) {
                data.splice(index, 1);
            }
            //console.log(data);
        },

        getContacts: function(){
            return data;
        },

        getUniqueCategories: function(){
            return createUniqueCategories(getAllCategories(data));
        },
    }

})();

var UIController = (function(){

    var DOMStrings = {
        listItemContainer: '.list-items',
        addContactBtn: '.add-contact-btn',
        deleteContactBtn: '.confirm-delete-btn',
        contactsContainer: '.contacts-container',
        addContactForm: '#addContactForm',
        addContactModal: '#addContactModal',
        deleteContactModal: '#deleteContactModal',
        inputName: '#name',
        inputSurname: '#surname',
        inputEmail: '#email',
        inputNumber: '#number',
        inputImage: '#image',
        messageContainer: '.message-container'
    };

    return {
        addHTMLCategoryTitle: function(el){
            var html, newHTML;
            html = '<div class="section-title">%cat%</div>';

            newHTML = html.replace('%cat%', el);
            newHTML = newHTML.replace('%catLetter%', el);

            document.querySelector(DOMStrings.contactsContainer).insertAdjacentHTML('beforeend', newHTML);
        },

        addHTMLListItem: function(obj){
            var html, newHTML;
            html =
                '<div id="contactId_%id%" class="row list-item align-items-center">' +
                '   <div class="col-auto">' +
                '       <div class="avatar">' +
                '           <img src="%imgUrl%" alt=""/>' +
                '       </div>' +
                '   </div>' +
                '   <div class="col list-item__details">' +
                '       <div class="name">%name% %surname%</div>' +
                '       <div class="number">%number%</div>' +
                '       <div class="email">%email%</div>' +
                '       <div class="email"></div>' +
                '   </div>' +
                '   <div class="col-auto list-item__actions">' +
                '       <button class="btn btn-link remove-contact-btn" data-id="%dataId%" data-toggle="modal" data-target="#deleteContactModal"><i class="fa fa-trash"></i></button>' +
                '   </div>' +
                '</div>';

            //replace placeholder text with data
            newHTML = html.replace('%id%', obj.id);
            newHTML = newHTML.replace('%dataId%', obj.id);
            newHTML = newHTML.replace('%imgUrl%', obj.imgUrl);
            newHTML = newHTML.replace('%name%', obj.name);
            newHTML = newHTML.replace('%surname%', obj.surname);
            newHTML = newHTML.replace('%number%', obj.number);
            newHTML = newHTML.replace('%email%', obj.email);

            // insert new HTML with replaced values
            document.querySelector(DOMStrings.contactsContainer).insertAdjacentHTML('beforeend', newHTML);
        },

        showAlert: function(status, message){
            var html, newHTML;
            html =
                '<div class="alert alert-%status% alert-dismissible fade show" role="alert">' +
                '%message%' +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</buttonn' +
                '</div>';

            newHTML = html.replace('%status%', status);
            newHTML = newHTML.replace('%message%', message);

            document.querySelector(DOMStrings.messageContainer).insertAdjacentHTML('beforeend', newHTML);

            setTimeout(function(){
                $('.alert').alert('close');
            }, 4000)

        },

        getDOMStrings: function(){
            return DOMStrings;
        },

        getInput: function () {
            // get form input
            var rawURL, newImgUrl;

            rawURL = document.querySelector(DOMStrings.inputImage).files[0];

            // check if file was uploaded, else use a placeholder.
            if(rawURL !== undefined) {
                // create blob url
                newImgUrl = URL.createObjectURL(rawURL);
            } else {
                newImgUrl = 'static/img/profile-placeholder.png';
            }

            return {
                name: document.querySelector(DOMStrings.inputName).value,
                surname: document.querySelector(DOMStrings.inputSurname).value,
                number: document.querySelector(DOMStrings.inputNumber).value,
                email: document.querySelector(DOMStrings.inputEmail).value,
                imgUrl: newImgUrl
            }
        },

        clearFields: function(){
            var fields, fieldsArr;
            fields = document.querySelectorAll(
                DOMStrings.inputName + ', ' +
                DOMStrings.inputSurname + ', ' +
                DOMStrings.inputNumber + ', ' +
                DOMStrings.inputEmail + ', ' +
                DOMStrings.inputImage
            );

            fieldsArr = Array.prototype.slice.call(fields);
            fieldsArr.forEach(function(current){
                current.value = "";
            });
        }
    }

})();

var appController = (function(contactCtrl, UICtrl){

    var displayCategorisedData = function(catArr, dataArr){
        var listContainer = document.querySelector(DOMStrings.contactsContainer);
        listContainer.innerHTML = "";

        if(!dataArr.length) {
            listContainer.insertAdjacentHTML('beforeend', '<p>No contacts.</p>')
        } else {
            catArr.forEach(function (current) {
                var catLetter = current;
                // console.log(current);
                UICtrl.addHTMLCategoryTitle(current);
                dataArr.forEach(function (current) {
                    if (current.category === catLetter) {
                        // console.log(current);
                        UICtrl.addHTMLListItem(current, current.category);
                    }
                });
            });
        }
    };

    var DOMStrings = UICtrl.getDOMStrings();

    var setupEventListeners = function(){

        window.addEventListener('load', function() {
            var forms, validation, input;

            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            forms = document.getElementsByClassName('needs-validation');

            // Loop over them and prevent submission
            validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        form.classList.add('was-validated');
                    } else {
                        input = UICtrl.getInput();

                        contactCtrl.addContact(input);

                        // update DOM
                        displayCategorisedData(contactCtrl.getUniqueCategories(), contactCtrl.getContacts());

                        // clear form fields.
                        UICtrl.clearFields();

                        form.classList.remove('was-validated');

                        $('#addContactModal').modal('hide');

                        //display success message
                        UICtrl.showAlert('success', 'Contact was added successfully.')
                    }
                    event.stopPropagation();
                    event.preventDefault();

                }, false);
            });
        }, false);

        //Delete a contact
        var deleteModal = $(DOMStrings.deleteContactModal);

        deleteModal.on('show.bs.modal', function (e) {
            document.querySelector(DOMStrings.deleteContactBtn).addEventListener('click', function(){
                //TODO: fix bug where this click event is running again for every new click. for example:
                //First delete item confirmation click: runA
                //Next  delete item conformation click: runA runB
                //Next  delete item conformation click: runA runB runC ?????
                var element = e.relatedTarget;
                var ID = parseInt(element.getAttribute('data-id'));

                console.log(ID);
                // remove from data
                contactCtrl.removeContact(ID);
                // update DOM
                displayCategorisedData(contactCtrl.getUniqueCategories(), contactCtrl.getContacts());
                //close modal
                deleteModal.modal('hide');

                UICtrl.showAlert('success', 'Contact was deleted successfully.')

            });
        });
    };

    return {
        init: function(){
            displayCategorisedData(contactCtrl.getUniqueCategories(), contactCtrl.getContacts());
            setupEventListeners();
        }
    }

})(contactController, UIController);

appController.init();
